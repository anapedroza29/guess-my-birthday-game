# from pickletools import read_unicodestring1
from random import randint
#It should only guess years between 1924 and 2004 including those years.

# Tries to guess your birth month and year with a prompt formatted like Guess «guess number» : «name» were you born in «m» / «yyyy» ? then prompts with "yes or no?"
# If the computer guesses it correctly because you respond yes, it prints the message I knew it! and stops guessing
# If the computer guesses incorrectly because you respond no, it prints the message Drat! Lemme try again! if it's only guessed 1, 2, 3, or 4 times. Otherwise, it prints the message I have other things to do. Good bye.

# The guess number, which starts at 1 (we need this because each guess starts with "Guess number")
# A random month number between 1 and 12
# A random year number between 1924 and 2004 (this was written in the requirements)

# player_name = prompt the player for their name
name = input("Hi! what is your name")

# month_number = generate a random number

# year_number = generate a random number


# Gess 1
for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print('Gess:', guess_number, "were you born in", 
    month_number, "/", year_number, "?")

    response = input("yes or no ?")


    # if response is "yes", then
    #     print the message "I knew it!"
    if response == 'yes':
        print("i knew it")
        exit() 
    elif guess_number == 5:
        print("I have other thing to do.Goo bye")

    # otherwise,
    #     print the message "Drat! Lemme try again!"
    else:
        print('Drat! Lemme try again!' )


